package sample;

import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public Button stop;
    public Button cancel;
    public Button start;
    public Button resume;
    public Label ticker;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        CountdownTimer countdownTimer = new CountdownTimer();
        countdownTimer.setDuration(5000);
        countdownTimer.setCountdownInterface(new CountdownTimer.CountdownInterface() {
            @Override
            public void onTick(int count) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        ticker.setText(String.valueOf(count));
                    }
                });
            }

            @Override
            public void onFinished() {
                System.out.println("Finished");
            }
        });
        start.setOnAction(event -> countdownTimer.start());
        stop.setOnAction(event -> countdownTimer.stop());
        resume.setOnAction(event -> countdownTimer.resume());
        cancel.setOnAction(event -> countdownTimer.cancel());
    }
}
